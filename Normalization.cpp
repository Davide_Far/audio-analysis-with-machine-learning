// Normalization.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "aquila/global.h"
#include "aquila/source/generator/SineGenerator.h"
#include "aquila/transform/FftFactory.h"
#include "aquila/tools/TextPlot.h"
#include "aquila/source/WaveFile.h"

#include<fstream>
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;
using namespace Aquila;

int main(int argc, char *argv[])
{
	int numeroCampioni;
	ofstream inputSignalValuesCSV;
	ofstream outputSignalValuesCSV;

	//Prende il file "applause" dal percorso e lo mette nella variabile "wav"
	WaveFile wav("C:\\Users\\David\\Desktop\\applause.wav");
	cout << "Caricato il file: " << wav.getFilename()
		<< " (" << wav.getBitsPerSample() << "b)" << std::endl;

	SampleType maxValue = 0, minValue = 0, average = 0;

	//Parametri segnale normalizzato
	SignalSource normAudio;
	SampleType reducedSample;
	vector <SampleType>normAudioSamples(323525);

	// Informazioni sul segnale di INPUT
	cout << "Numero dei samples di applause: " << wav.getSamplesCount() <<endl;
	cout << "Dimensione del file: " << wav.getWaveSize() << endl;
	cout << "Bit per campione del file: " << wav.getBitsPerSample() << endl;
	cout << "Numero di canali: " << wav.getChannelsNum() << endl;
	cout << "Frequenza di campionamento del file: " << wav.getSampleFrequency() << endl;

	for (std::size_t i = 0; i < wav.getSamplesCount(); ++i)
	{
		if (wav.sample(i) > maxValue)
			maxValue = wav.sample(i);
	}
	cout << "Valore del sample piu' alto: " << maxValue << std::endl;

	for (std::size_t i = 0; i < wav.getSamplesCount(); ++i)
	{
		if (wav.sample(i) < minValue)
			minValue = wav.sample(i);
	}
	cout << "Valore del sample piu' basso: " << minValue << std::endl;

	for (auto sample : wav)
	{
		average += sample;
	}
	average /= wav.getSamplesCount();
	cout << "Media dei valori: " << average << std::endl;

	cout << "==========================================================================================" << endl;
	cout << "Quanti campioni si desiderano esportare?" << endl;
	cin >> numeroCampioni;


	// Costruzione del segnale ridotto
	
	//Apertura dei file di scrittura
	inputSignalValuesCSV.open("inputSignalValues.txt");

	for (std::size_t i = 0; i < wav.getSamplesCount(); ++i)
	{
		reducedSample = wav.sample(i);

		//1 - Variabile temp = valore del sample + applicazione riduzione
		reducedSample = reducedSample/2.0f;

		//2 - Salvo TEMP nell'array dei vettori
		normAudioSamples[i] = reducedSample;
	}

	normAudio = SignalSource(normAudioSamples,wav.getSampleFrequency());

	//Importo i valori del segnale all'interno del documento
	inputSignalValuesCSV << "X;Y;\n";
	if (inputSignalValuesCSV.is_open()) {
		for (int i = 0; i < numeroCampioni; i++) {
			inputSignalValuesCSV << i << ";" << wav.sample(i) << ";\n";
		}
	}
	else
		cout << "Errore nell'apertura del file d'output!" << endl;
	inputSignalValuesCSV.close();

	cout << "========================================================" << endl;
	// Informazioni sul segnale di OUTPUT
	cout << "Numero dei samples di applause: " << normAudio.getSamplesCount() << endl;
	cout << "Bit per campione del file: " << normAudio.getBitsPerSample() << endl;
	cout << "Frequenza di campionamento del file: " << normAudio.getSampleFrequency() << endl;

	maxValue = 0; 
	minValue = 0; 
	average = 0;

	for (size_t i = 0; i < normAudio.getSamplesCount(); ++i)
	{
		if (normAudio.sample(i) > maxValue)
			maxValue = normAudio.sample(i);
	}

	cout << "Valore del sample piu' alto: " << maxValue << std::endl;

	for (size_t i = 0; i < normAudio.getSamplesCount(); ++i)
	{
		if (normAudio.sample(i) < minValue)
			minValue = normAudio.sample(i);
	}

	cout << "Valore del sample piu' basso: " << minValue << std::endl;

	for (auto sample : normAudio)
	{
		average += sample;
	}
	average /= normAudio.getSamplesCount();
	cout << "Media dei valori: " << average << std::endl;

	//Importo i valori del segnale normalizzato all'interno del documento di OUTPUT
	outputSignalValuesCSV.open("outputSignalValues.txt");
	outputSignalValuesCSV << "X;Y;\n";
	if (outputSignalValuesCSV.is_open()) {
		for (int i = 0; i < numeroCampioni; i++) {
			outputSignalValuesCSV << i << ";" << normAudioSamples[i] << ";\n";
		}
	}
	else
		cout << "Errore nell'apertura del file d'output!" << endl;
	outputSignalValuesCSV.close();

	system("pause");
	return 0;
}